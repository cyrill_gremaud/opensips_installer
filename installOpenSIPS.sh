#!/bin/bash

# This script helps to install OpenSIPS server for e-SIS project
#
# @Author Cyrill Gremaud (gremaudc@gmail.com)
# @Date 06.07.2015
#
# bitbucket URL: https://cyrill_gremaud@bitbucket.org/cyrill_gremaud/opensips_installer.git
#

set -o pipefail > /dev/null 2>&1
if [[ $? -ne 0 ]]; then  echo -e "\e[31m[!]-> Warning ! Impossible to set pipefail. Don't use pipeline command in this script (cmd1 | cmd2) \e[0m"; fi

set -o nounset

# Customize these variables
opensipsRelease="1.11"		#relative to OpenSIPS git version

# Constants
DL_SRC_DIR="/usr/local/src"
INSTALL_DIR="/usr/local/opensips_server"			#Please don't change it
GIT_URL="https://github.com/OpenSIPS/opensips.git"
PATCHES_DIR=$(pwd)"/patches"

# Called in case of important error
function quit_on_error {
	echo -e "\e[41m[!]-> The program quits due to important error(s). Correct it before.\e[0m"
	echo -e "\e[41m[$(date +'%Y-%m-%d@%H:%M:%S')] Error information => $@\e[0m" >&2
	clean_option
}

function clean_option {
	echo -e "\e[93m[+] Cleaning the system\e[0m"
	echo -e "\e[4m[!] Please be careful about your next answers\e[0m"
	rm -rf $DL_SRC_DIR/opensips_$opensipsRelease

	#todo: warn and ask user about --purge. let the choice to user
	read -p "Do you really want to delete all mysql information (including databases and configuration) [y=yes][n=no]" answer
	if [[ "$answer" != "y" || "$answer" == "yes" ]]; then
		apt-get remove --purge mysql-server libmysqlclient-dev --yes > /dev/null 2>&1
	else
		echo -e "\e[32m[!]-> MySQL stuff has not been unistalled. If you have trouble with the installation, please consider to remove all MySQL stuff.\e[0m"
	fi
	apt-get remove --purge m4 flex bison libncurses-dev subversion linux-headers-server build-essential libssl-dev libcurl4-openssl-dev --yes > /dev/null 2>&1
	apt-get autoremove --yes > /dev/null 2>&1
	apt-get autoclean --yes > /dev/null 2>&1
	exit 1
}

function print_out {
	defaultLevel="INFO"
	#todo
}

# Parse arguments
while getopts ":cdhr:" FLAG; do
  case $FLAG in
	c)	#Clean the system by removing all installed components
		clean_option "Cleaning the system"
	;;
    d)  #set option "d"
		set -o xtrace
	;;
	r)  #set the OpenSIPS git release to get
		opensipsRelease=$OPTARG
		echo -e "\e[31m[!]-> Be careful using - option. This script has been developed for OpenSIPS release 1.11\e[0m"
	;;
    h)  #show help
		echo ""
		echo "This script helps to install OpenSIPS server for e-SIS project. If the option -r is specified, the script clean the system"
		echo ""
		echo "Usage:"
		echo " -c : Clean the system"
		echo " -r : eg. -r 1.10 to install OpenSIPS 1.10. [experimental]"
		echo " -d : Enable debugging"
		echo " -h : This help message"
		echo ""
		exit 1
	;;
    \?) #unrecognized option - show help
		echo "This argument is not recognized, try -h to show help"
		exit 2
	;;
  esac
done

echo -e "\e[44m[+] This script helps to install OpenSIPS server with TLS module and LDAP ready       \e[0m"
echo -e "\e[44m[+] ----------------------------------------------------------------------------------\e[0m"
echo -e "\e[44m[+] Please read well each colorized text. Some things must be done manually           \e[0m"
echo -e "\e[44m[+] This script has been developped for OpenSIPS 1.11. Be Careful with other releases \e[0m"
echo -e "\e[44m[+]                                                                                   \e[0m"
echo -e "\e[44m[+] If possible, it is better to install OpenSIPS to a dedicated server !             \e[0m"
echo -e "\e[44m[+] Author: Cyrill Gremaud (gremaudc@gmail.com)                                       \e[0m"
echo -e "\e[44m[+]                                                                                   \e[0m"

# Make sure that the script is run by root user
if [[ $EUID -ne 0 ]]; then quit_on_error "This script must be run as root"; fi

# Make sure that no existing install will interfere with this one
echo -e "\e[93m[+] Check system to be sure that it is clean to start the installation\e[0m"
rm -rf $INSTALL_DIR
rm -rf $DL_SRC_DIR
rm -f /etc/init.d/opensips
rm -f /etc/default/opensips
rm -f /var/log/opensips.log
killall opensips > /dev/null 2>&1

# Update the server
echo -e "\e[93m[+] Please wait during OS update\e[0m"
dpkg --configure -a > /dev/null 2>&1
apt-get update > /dev/null 2>&1
apt-get upgrade --yes > /dev/null 2>&1
if [[ $? -ne 0 ]]; then  echo -e "\e[31m[!]-> Warning ! Problem during the update. Be careful.\e[0m"; fi

# Install some perquisites
echo -e "\e[93m[+] Please wait during Installing perquisites\e[0m"

# Fucking trick to be able to set the desired mysql root password
apt-get remove --purge mysql-* --yes > /dev/null 2>&1
rm -rf /var/lib/mysql
export DEBIAN_FRONTEND="noninteractive"
read -p "Please choose a MySQL root password: " mysqlRootPassword
debconf-set-selections <<< "mysql-server mysql-server/root_password password "$mysqlRootPassword
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password "$mysqlRootPassword
apt-get install mysql-server m4 flex bison libncurses-dev git subversion linux-headers-server build-essential --yes > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Please check the installation of perquisites"; fi

# Install cryptographic stuff needed for TLS module
echo -e "\e[93m[+] Please wait during Installing cryptographic stuff needed by TLS module\e[0m"
apt-get install libmysqlclient-dev libssl-dev libcurl4-openssl-dev --yes > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Problem during installation of cryptographic stuff"; fi

# Get sources of OpenSIPS
echo -e "\e[93m[+] Please wait during downloading OpenSIPS $opensipsRelease sources\e[0m"
if [[ ! -d $DL_SRC_DIR ]]; then
	mkdir $DL_SRC_DIR
	if [[ $? -ne 0 ]]; then quit_on_error "$DL_SRC_DIR cannot be created. Please check permissions. Are you root ?"; fi
fi

cd $DL_SRC_DIR > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "$DL_SRC_DIR cannot be accessed. Please check permissions"; fi

git clone -b $opensipsRelease $GIT_URL opensips_$opensipsRelease > /dev/null 2>&1

if [[ ! -d opensips_$opensipsRelease ]]; then 
	quit_on_error "Error during the Git Clone operation. Check if $opensips_$opensipsRelease exist and its content"; 
fi

# Configure opensips settings for compilation
cd $DL_SRC_DIR/opensips_$opensipsRelease > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "$DL_SRC_DIR/opensips_$opensipsRelease cannot be accessed. Please check permissions"; fi

echo -e "\e[93m[+] Configuration and Compilation of OpenSIPS. Go to following menus\e[0m"
echo -e "\e[32m[!]-> 1. Configure Compile Options\e[0m"
echo -e "\e[32m[!]-> 2. Configure Compile Flags -> USE_TLS\e[0m"
echo -e "\e[32m[!]-> 3. Configure Excluded Modules -> db_mysql, ldap\e[0m"
echo -e "\e[91m[!]-> 4. Configure Install prefix -> $INSTALL_DIR (very important to set this path!!!)\e[0m"
echo -e "\e[32m[!]-> 5. Don't forget to save your modifications\e[0m"
echo -e "\e[32m[!]-> 6. Go back twice and select 'Compile and Install OpenSIPS'\e[0m"
echo -n "Press Enter to continue"
read

# todo: maybe run make all instead of using menuconfig but need to know how to set installation dir, activate flag, module etc...
make menuconfig

if [[ -d $INSTALL_DIR ]]; then
	if [[ ! -d $INSTALL_DIR/etc || ! -d $INSTALL_DIR/sbin || ! -d $INSTALL_DIR/share ]]; then
		quit_on_error "$INSTALL_DIR does not contains desired directories. Are you sure that the install prefix is set correctly ?"; 
	fi
else
	quit_on_error "$INSTALL_DIR not found. Are you sure that the install prefix is set correctly ?"
fi

# Create the openSIPS mysql database and configuration
echo -e "\e[93m[+] Installing MySQL Server and configuring OpenSIPS\e[0m"
defaultdbname="opensips"

#remove # in various variables
patch $INSTALL_DIR/etc/opensips/opensipsctlrc < $PATCHES_DIR/opensipsctlrc.patch > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during applying patch on $INSTALL_DIR/etc/opensips/opensipsctlrc"; fi

echo -e "\e[32m[!]-> Use default values for OpenSIPS mysql settings ? [Y=yes][N=no]\e[0m"
read answer
if [[ $answer = "N" || $answer = "n" ]]; then
	# get custom values from user input
	read -p "DBENGINE: " dbengine
	read -p "DBHOST: " dbhost
	read -p "DBNAME: " dbname
	defaultdbname=dbname
	read -p "DBRWUSER: " dbrwuser
	read -p "DBDBRWPW: " dbrwpw
	read -p "DBROOTUSER: " dbrootuser
	
	# we use sed instead of .patch because we need to customize the values with user input
	sed -i 's/DBENGINE=MYSQL/DBENGINE='$dbengine/ $INSTALL_DIR/etc/opensips/opensipsctlrc
	sed -i 's/DBHOST=localhost/DBHOST='$dbhost/ $INSTALL_DIR/etc/opensips/opensipsctlrc
	sed -i 's/DBNAME=opensips/DBNAME='$dbname/ $INSTALL_DIR/etc/opensips/opensipsctlrc
	sed -i 's/DBRWUSER=opensips/DBRWUSER='$dbrwuser/ $INSTALL_DIR/etc/opensips/opensipsctlrc
	sed -i 's/DBDBRWPW="opensipsrw"/DBDBRWPW='$dbrwpw/ $INSTALL_DIR/etc/opensips/opensipsctlrc
	sed -i 's/DBROOTUSER="root"/DBROOTUSER='$dbrootuser/ $INSTALL_DIR/etc/opensips/opensipsctlrc
	
	echo -e "\e[32m[!]-> Your values: $dbengine, $dbhost, $dbname, $dbrwuser, $dbrwpw, $dbrootuser have been set in $INSTALL_DIR/etc/opensips/opensipsctlrc\e[0m"
fi

mysql -u root -p$mysqlRootPassword -e "DROP DATABASE IF EXISTS "$defaultdbname";"

echo -e "\e[32m[!]-> Say no for each extra tables\e[0m"
$INSTALL_DIR/sbin/opensipsdbctl create			#create opensips mysql database and tables

# Configuration and installation of residential script
echo -e "\e[93m[+] Configuration and installation of residential script\e[0m"
echo -e "\e[32m[!]-> 1. Go to Generate OpenSIPS scripts -> Residential Script -> Configure Residential Script and select these options\e[0m"
echo -e "\e[32m[!]-> 2. ENABLE_TCP, ENABLE_TLS, USE_ALIASES, USE_AUTH, USE_DBUSRLOC, USE_DIALOG\e[0m"
echo -e "\e[32m[!]-> 3. Then, come back and select Generate Residential Script\e[0m"
echo -n "Press Enter to continue"
read
$INSTALL_DIR/sbin/osipsconfig
mv $INSTALL_DIR/etc/opensips/opensips_residential* $INSTALL_DIR/etc/opensips/opensips_running_config.cfg	#rename residential script
if [[ $? -ne 0 ]]; then quit_on_error "Cannot rename generated residential script. Please be sure to have just one generated !"; fi

# Modifying the residential script
echo -e "\e[93m[+] Modifying the residential script\e[0m"
patch $INSTALL_DIR/etc/opensips/opensips_running_config.cfg < $PATCHES_DIR/opensipsResidential2.patch > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during applying patch on $INSTALL_DIR/etc/opensips/opensips_running_config.cfg"; fi
echo -e "\e[41m[!] Don't forget to configure $INSTALL_DIR/etc/opensips/opensips_running_config.cfg in accordance with your server configuration\e[0m"

# Configuration of the Init script
echo -e "\e[93m[+] Configuration of init script\e[0m"
cp $DL_SRC_DIR/opensips_$opensipsRelease/packaging/debian/opensips.init /etc/init.d/opensips
chmod +x /etc/init.d/opensips
patch /etc/init.d/opensips < $PATCHES_DIR/opensipsInit.patch > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during applying patch on /etc/init.d/opensips"; fi

# Configuration of default behaviour
echo -e "\e[93m[+] Configuration of default starting behaviour\e[0m"
cp $DL_SRC_DIR/opensips_$opensipsRelease/packaging/debian/opensips.default /etc/default/opensips
patch /etc/default/opensips < $PATCHES_DIR/opensipsDefault.patch > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during applying patch on /etc/default/opensips"; fi

# Configuration of Syslog
echo -e "\e[93m[+] Configuration of Syslog\e[0m"
patch --dry-run --silent /etc/rsyslog.conf < $PATCHES_DIR/rsyslog.patch > /dev/null 2>&1
if [[ $? -eq 0 ]]; then
	patch -f /etc/rsyslog.conf < $PATCHES_DIR/rsyslog.patch > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Error during applying patch on /etc/rsyslog.conf"; fi
fi
touch /var/log/opensips.log
chmod 777 /var/log/opensips.log
/etc/init.d/rsyslog restart

# Create template for LDAP configuration file
echo -e "\e[93m[+] Creation $INSTALL_DIR/etc/opensips/ldap.conf template.\e[0m"
touch $INSTALL_DIR/etc/opensips/ldap.conf
echo "[sipaccounts]" > $INSTALL_DIR/etc/opensips/ldap.conf
echo "ldap_server_url = \"ldap://127.0.0.1/\"" >> $INSTALL_DIR/etc/opensips/ldap.conf
echo "ldap_version = 3" >> $INSTALL_DIR/etc/opensips/ldap.conf
echo "ldap_bind_dn = \"cn=admin,dc=e-sis,dc=org\"" >> $INSTALL_DIR/etc/opensips/ldap.conf
echo "ldap_bind_password = \"mysuperldappassword\"" >> $INSTALL_DIR/etc/opensips/ldap.conf

# Try to start openSIPS server
echo -e "\e[93m[+] Try to start OpenSIPS server\e[0m"
/etc/init.d/opensips start > /dev/null 2>&1
if [[ -f /var/run/opensips/opensips.pid ]]; then
	pid=$(cat /var/run/opensips/opensips.pid)
	echo -e "\e[32m[!]-> OpenSIPS started successfully with pid $pid\e[0m"
	echo -e "\e[32m[!]-> Now, shutting down the server to configure it manually\e[0m"
	/etc/init.d/opensips stop > /dev/null 2>&1
else
	echo -e "\e[31m[!]-> OpenSIPS not started. Please check the syslog\e[0m"
	read -p "Press Enter to show opensips log"
	cat /var/log/syslog | egrep "opensips"
fi

# Cleaning stuff
echo -e "\e[93m[+] Cleaning the system\e[0m"
rm -rf $DL_SRC_DIR/opensips_$opensipsRelease
apt-get autoremove --yes > /dev/null 2>&1
apt-get autoclean --yes > /dev/null 2>&1
echo -e "\e[32m[!]-> Installation done correctly\e[0m"
echo -e "\e[31m[!]-> Your OpenSIPS server seems running but you must have error because the configuration of $INSTALL_DIR/etc/opensips/ldap.conf is just a template. Please configure this file correctly and try to restart the server\e[0m"