# README #

### Installation helper for OpenSIPS ###

* This script helps to install OpenSIPS server for e-SIS project

### How do I get set up? ###

Follow these how-to to run this script.

```
#!shell
sudo -s
cd /usr/local
git clone https://cyrill_gremaud@bitbucket.org/cyrill_gremaud/opensips_installer.git
chmod -R 700 opensips_installer
cd opensips_installer
./installOpensips.sh
```

You can display the help of the script using the -h option

```
#!shell
./installOpensips.sh -h
This script helps to install OpenSIPS server for e-SIS project. If the option -r is specified, the script clean the system

Usage:
 -c : Clean the system
 -r : eg. -r 1.10 to install OpenSIPS 1.10. [experimental]
 -d : Enable debugging
 -h : This help message
```

Do not hesitate to contact me for bug or features requests